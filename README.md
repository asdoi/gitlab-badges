# GitLab Badges
Examples of badges that can be generated with the gitlab API v4:

### Generated with shields.io
[![Release](https://img.shields.io/badge/dynamic/json.svg?label=Release&url=https://gitlab.com/api/v4/projects/19240825/releases&query=0.name&colorB=brightgreen&logo=gitlab)]()
[![Stars](https://img.shields.io/badge/dynamic/json.svg?style=social&label=Stars&url=https://gitlab.com/api/v4/projects/19240825&query=star_count&logo=gitlab)]()
[![Forks](https://img.shields.io/badge/dynamic/json.svg?label=Forks&url=https://gitlab.com/api/v4/projects/19240825&query=forks_count&colorB=important)]()  
[![Issues](https://img.shields.io/badge/dynamic/json.svg?label=Issues&url=https://gitlab.com/api/v4/projects/19240825/issues_statistics&query=statistics.counts.all&colorB=informational)]()
[![Open Issues](https://img.shields.io/badge/dynamic/json.svg?label=Open%20issues&url=https://gitlab.com/api/v4/projects/19240825/issues_statistics&query=statistics.counts.opened&colorB=critical)]()
[![Closed Issues](https://img.shields.io/badge/dynamic/json.svg?label=Closed%20issues&url=https://gitlab.com/api/v4/projects/19240825/issues_statistics&query=statistics.counts.closed&colorB=green)]()   
[![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/19240825?license=true&query=license.key&colorB=yellow)]()
[![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/19240825?license=true&query=license.name&colorB=yellow)]()  
[![ID](https://img.shields.io/badge/dynamic/json.svg?label=ID&url=https://gitlab.com/api/v4/projects/19240825&query=id&colorB=blueviolet)]()
[![Owner](https://img.shields.io/badge/dynamic/json.svg?label=Owner&url=https://gitlab.com/api/v4/projects/19240825&query=namespace.name&colorB=yellowgreen)]()  
[![Name](https://img.shields.io/badge/dynamic/json.svg?label=Name&url=https://gitlab.com/api/v4/projects/19240825&query=name&colorB=informational)]()
[![Description](https://img.shields.io/badge/dynamic/json.svg?label=Description&url=https://gitlab.com/api/v4/projects/19240825&query=description&colorB=informational)]()  
[![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/19240825&query=created_at&colorB=informational)]()
[![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/19240825&query=last_activity_at&colorB=informational)]()  
[![Default branch](https://img.shields.io/badge/dynamic/json.svg?label=Default%20branch&url=https://gitlab.com/api/v4/projects/19240825&query=default_branch&colorB=green)]()  
[![Last Release](https://img.shields.io/badge/dynamic/json.svg?label=Last%20release&url=https://gitlab.com/api/v4/projects/19240825/releases&query=0.released_at&colorB=brightgreen)]()
[![Tag](https://img.shields.io/badge/dynamic/json.svg?label=Tag&url=https://gitlab.com/api/v4/projects/19240825/repository/tags&query=0.name&colorB=brightgreen&logo=gitlab)]()
[![Last Tag](https://img.shields.io/badge/dynamic/json.svg?label=Last%20tag&url=https://gitlab.com/api/v4/projects/19240825/repository/tags&query=0.commit.created_at&colorB=brightgreen)]()
[![Last Commit](https://img.shields.io/badge/dynamic/json.svg?label=Last%20commit&url=https://gitlab.com/api/v4/projects/19240825/repository/commits&query=0.created_at&colorB=red)]()

### Generated from GitLab
[![pipeline status](https://gitlab.com/asdoi/gitlab-badges/badges/master/pipeline.svg)]()
[![pipeline status](https://gitlab.com/asdoi/gitlab-badges/badges/master/coverage.svg)]()

# Also see this, where I combined this with other static badges: [All Branch](https://gitlab.com/asdoi/gitlab-badges/-/tree/all) & [Git Badges](https://gitlab.com/asdoi/git_badges)

### Usage:
Replace this part `https://gitlab.com/api/v4/projects/19240825` in the image links with the link to your own project.

### A collection of articles how you can add your own badges to your gitlab repository:
 - https://gitlab.com/asdoi/git_badges
 - https://github.com/elbosso/gitlabsvgbadges
 - https://gist.github.com/elbosso/4f3bb0fb95dd1dc499cd46db422900bf
 - https://stackoverflow.com/questions/45756371/gitlab-ci-using-badges-for-each-job
 - https://stackoverflow.com/questions/50605421/how-to-create-repository-badges-in-gitlab
 - https://gitlab.com/kubouch/ci-test/-/tree/badges
 - https://gitlab.version.fz-juelich.de/vis/jusense-cicd
 - https://gitlab.version.fz-juelich.de/vis/jusense-cicd/-/wikis/Continuous-Integration-and-Delivery#badges
 - https://gitlab.com/gitlab-org/gitlab-foss/-/issues/50603
 - https://dev.to/ananto30/how-to-add-some-badges-in-your-git-readme-github-gitlab-etc-3ne9
 - https://github.com/badges/shields/issues/541
 - https://elbosso.github.io/dynamische_gitlab_badges_iv.html
